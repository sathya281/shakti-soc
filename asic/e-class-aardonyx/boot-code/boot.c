#include "qspi_aardonyx.h"

#define MEM_TYPE_IS25LP256_ID 0x9d189d18
#define SIZE 16038
char fail_bit = 0;
int status = 0;

int check_fail_bit(){
  if(fail_bit){
        fail_bit = 0;
        return -1;
  }
    else{
        fail_bit = 0;
        return 0;
    }
}

int ExitXIP(){
    set_qspi_shakti32(cr, CR_ABORT);
//    printf("\t ABORT GENERATED \n");
    waitfor(100);
    qspi_init(27,0,3,1,15,1);
    return 0;
}


void jumpToSDRAM(){
    asm volatile("fence.i");
//    printf("\t BootMgr: Fence Complete \n\t ---BOOTING LINUX--- \n");
    asm volatile( "li x30, 0x80000000" "\n\t"
                  "jr x30" "\n\t"
                 :
                 :
                 :"x30","cc","memory"
                                           
               );
  }


int flashIdentificationDevice(){
//rintf("\tReading the ID register and discovering the Flash Device\n");
    set_qspi_shakti32(dlr,4);
    set_qspi_shakti32(ccr,(CCR_FMODE(CCR_FMODE_INDRD)|CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0x90)|CCR_ADSIZE(THREEBYTE)|CCR_ADMODE(SINGLE)|CCR_DMODE(SINGLE)));
    set_qspi_shakti32(ar, 0);
    status = 0; // Useless Variable but still!!!!
    int ret = wait_for_tcf(status);
    int value = get_qspi_shakti(dr);
//  	printf("\t device id %x\n",value);
    reset_interrupt_flags();
    if(value == MEM_TYPE_IS25LP256_ID){
//    	printf("\tN25Q256 Device Detected \n");
    }
    else{
//    	printf("\t Device Not Detected - Diagnose %08x\n",value);
    	return -1;
    }
}

int flashMemInit(){   //Supposedly a set of routines to check if the memory/interface or whatever is proper
	int ret = flashIdentificationDevice();
	if(ret==-1){
//		printf("Flash Mem Init Failed -- Quitting Program, Diagnose");
		return ret;
	}
	else //printf("Flash Mem Init Success\n");
    return 0;
	//to fill in code
}

int flashReadStatusRegister(){
//    printf("\tReading the Status bits of the Flash\n");
    set_qspi_shakti32(dlr,4);
    set_qspi_shakti32(ccr,(CCR_FMODE(CCR_FMODE_INDRD)|CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0x05)|CCR_DMODE(SINGLE)));
 	status = 0;
	waitfor(1000);
    int ret = wait_for_tcf(status);
    waitfor(100);
    int value = get_qspi_shakti(dr);
//	printf("Read status register value %08x \n", value);
    reset_interrupt_flags();
    if(ret){
//        printf("\tRead Status Register Failed\n");
		fail_bit = 1;
        return -1;
        }
    else 
    	return value;
}

int flashWriteEnable(){
//    printf("\tWrite Enable\n");
    set_qspi_shakti32(ccr,(CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0x06)));
    int ret = wait_for_tcf(0); //Indicating the completion of command -- Currently polling
    reset_interrupt_flags();
    return ret; 
}

int flashEnable4ByteAddressingMode(){  //Enable 4-byte addressing Mode and read the status to verify that it has happened correctly

    if(flashWriteEnable()){
   //     printf("\t Write Enable Failed \n");
        return -1;
    }
    waitfor(100);
    set_qspi_shakti32(ccr,(CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0xB7)));
  //  int status =0; 
    status = 0;
    int ret = wait_for_tcf(status);
    reset_interrupt_flags();
    waitfor(100);
    //Checking Phase
    status = flashReadStatusRegister();
//    printf("\t Status Value: %08x\n",status);
}


//QUAD SPI DDR XIP:

int flashQuadSPIDDRXip(int addr, int* dest_addr){
    if(flashWriteVolatileConfigReg(0x40404040)){
//        printf("\t Volatile Configuration Register not Set -- Diagnose\n");
        return -1;
    }
	 wait_for_wip();
//status = wait_for_wip();
//	printf("\t qspi status register %08x\n",status);
    reset_interrupt_flags();
    set_qspi_shakti32(ccr,(CCR_DDRM|CCR_FMODE(CCR_FMODE_INDRD)|CCR_DMODE(QUAD)|CCR_DUMMY_CONFIRMATION|CCR_DCYC(10)|CCR_ADSIZE(FOURBYTE)|CCR_ADMODE(QUAD)|CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0xEE)));
    set_qspi_shakti32(dcr,(DCR_FSIZE(27)|DCR_MODE_BYTE(0xA0)|DCR_CKMODE(1)));
    set_qspi_shakti32(dlr,0x4);
    set_qspi_shakti32(ar, addr); //Address where the Config_string is situated in the micron nand flash memory
    status=0;
    wait_for_tcf(status);
    waitfor(100); 
    *dest_addr = get_qspi_shakti(dr);
    reset_interrupt_flags(); 
//    printf("\t Trying XIP now\n");
    set_qspi_shakti32(ccr, (CCR_DDRM|CCR_FMODE(CCR_FMODE_MMAPD)|CCR_DMODE(QUAD)|CCR_DUMMY_CONFIRMATION|CCR_DCYC(10)|CCR_ADSIZE(FOURBYTE)|CCR_ADMODE(QUAD)|CCR_IMODE(NDATA)));
    set_qspi_shakti32(dcr,(DCR_FSIZE(27)|DCR_MODE_BYTE(0xA0)|DCR_CKMODE(1))); 
    waitfor(25);
	return 0;
}



int flashWriteVolatileConfigReg(int value){
//    printf("\t Setting Volatile Configuration Register with the Value: %08x\n",value);
	if(micron_write_enable(status)){
    //    printf("Panic: Write Enable Command Failed to execute\n");
        return -1;
	}
     reset_interrupt_flags();
//	flashReadStatusRegister();
    set_qspi_shakti32(dlr,DL(1));
    set_qspi_shakti8(dr,value);  //The value to be written into the VECR register to enable XIP. Indicating XIP to operate in Quad Mode
    set_qspi_shakti32(ccr,(CCR_FMODE(CCR_FMODE_INDWR)|CCR_DMODE(SINGLE)|CCR_IMODE(SINGLE)|CCR_INSTRUCTION(0x01)));
    waitfor(50);
    status=0;
    int ret = wait_for_tcf(status);
//    printf("Status : %d dcr : %d cr : %d ccr : %d dlr: %d dr: %d\n",status,*dcr,*cr,*ccr,*dlr,*dr);
    reset_interrupt_flags();
    waitfor(50);  //Giving Micron time to store the data
    return ret;
}


int wait_for_wip(){
    int status1;
    do{
//        printf("\t Waiting for Wip \n");
        status1 = flashReadStatusRegister(0x05);
        if(check_fail_bit())
            return -1;
        if(status1 & 0x0){
//            printf("\tQSPI: Programming Error - Diagnose\n");
            return -1;
        }
        waitfor(20000);
    }while(status1 & 0x01);
//    printf("\t QSPI: Page Program/Erase Command Successfully Completed\n");
    return 0;
}	

int main()
{
    qspi_init(27,0,3,1,15,1);
    int ar_read,i,j;
    waitfor(400); //Time for Micron to start, maybe?
  if(flashMemInit()) //Because of STARTUPE2 primitive, the run fails for the first time it is programmed since three clock cycles are skipped. Run again
        return -1;  //Didn't work
    int xip_value = 0x40404040;


  flashEnable4ByteAddressingMode();

  int* sdram_address = (int*) 0x80010000;
  int* xip_address = (int*) 0x90000004; //Read data register after the first element is read through QUAD mode
	flashQuadSPIDDRXip(0x000000, *(sdram_address));  //Quad mode DDR
    sdram_address++;
    int dum_data;
    for(i=0;i<SIZE;++i) {
         dum_data = get_qspi_shakti(*(xip_address));
         waitfor(100);
         *(sdram_address) = dum_data;
         sdram_address++;
         xip_address++;
         waitfor(100);
         reset_interrupt_flags();
         waitfor(10);
	}
 //  printf("\n\t----COPIED TO SDRAM---- \n"); 
    ExitXIP();
// JUMPING TO SDRAM 0x80000000    
    jumpToSDRAM();
 
//	return 0;

}
